import sys, json, base64, re
import requests

#This script scans all issues on a Jira instance and creates a Gitlab branch and merge request 
#for those that do not already have one, and that have the "In Progress" status category.

#It must be launched by the .gitlab-ci.yml file, at the root of the repository,
#and takes 4 arguments:
#$CI_PROJECT_ID $GITLAB_API_TOKEN $JIRA_EMAIL $JIRA_API_TOKEN
#$CI_PROJECT_ID is automatically defined by Gitlab.
#The other 3 arguments must have been defined and masked through the Gitlab project UI in Settings > CI/CD > Variables.

if len(sys.argv) < 5:
	sys.exit("Too few arguments. Please provide CI_PROJECT_ID, GITLAB_API_TOKEN, JIRA_EMAIL and JIRA_API_TOKEN")

#Gitlab credentials
GITLAB_PROJECT_ID = sys.argv[1] #Current Project ID
GITLAB_API_TOKEN = sys.argv[2]

#Jira credentials
#Replace this with your Jira instance URL:
JIRA_URL = "https://integration-test-dotscreen.atlassian.net"
JIRA_PROJECT_KEY = "GI"
JIRA_EMAIL = sys.argv[3]
JIRA_API_TOKEN = sys.argv[4]
ENCODED_JIRA_AUTH = (base64.b64encode((f"{JIRA_EMAIL}:{JIRA_API_TOKEN}").encode("utf-8"))).decode("utf-8")

#Base URLs for API requests
GITLAB_API_BASE_URL = "https://gitlab.com/api/v4"
JIRA_API_BASE_URL = f"{JIRA_URL}/rest/api/latest"




#List of all Jira issues
search_results = requests.get(f"{JIRA_API_BASE_URL}/search?jql=project={JIRA_PROJECT_KEY}", headers={"Authorization": f"Basic {ENCODED_JIRA_AUTH}"}).json()
jira_issues = search_results["issues"]

#List of all Gitlab branches
branches = set()

branches_request = requests.get(f"{GITLAB_API_BASE_URL}/projects/{GITLAB_PROJECT_ID}/repository/branches?per_page=100&private_token={GITLAB_API_TOKEN}")
#Only 100 results per page can be returned by Gitlab API 
pages_number = branches_request.headers["X-Total-Pages"]
for page in range(1, int(pages_number) + 1):
	branches_request = requests.get(f"{GITLAB_API_BASE_URL}/projects/{GITLAB_PROJECT_ID}/repository/branches?per_page=100&page={page}&private_token={GITLAB_API_TOKEN}")
	for branch in branches_request.json():
		branches.add(branch["name"])

print(f"Existing branches: {branches}")
print(" ")

#For every newly-created Jira issue with status "In Progress", we will create a related branch and merge request in Gitlab.
for issue in jira_issues:
	issue_key = issue["key"]
	issue_title = issue["fields"]["summary"]
	issue_link = f"{JIRA_URL}/browse/{issue_key}"
	issue_status_category = issue["fields"]["status"]["statusCategory"]["name"]

	#We only process In Progress issues
	if issue_status_category == "In Progress":
		print(f"Checking Jira issue \"{issue_key} - {issue_title}\" ({issue_status_category})...")

		#We format the branch name to lower case with no special characters or spaces
		branch_name = issue_key.lower() + "-" + re.sub("[^A-Za-z0-9 ]+", "", issue_title).lower().replace(" ", "-")

		#If the issue doesn't have a related branch
		if not branch_name in branches:
			print(f"Creating branch {branch_name}...")

			#We create a branch.
			branch_creation = requests.post(f"{GITLAB_API_BASE_URL}/projects/{GITLAB_PROJECT_ID}/repository/branches?private_token={GITLAB_API_TOKEN}",
				data={
					"branch": branch_name,
					"ref": "master"
				})
			print(branch_creation.text)

			#If the branch has no merge requests (extra check), we create one.
			merge_requests = requests.get(f"{GITLAB_API_BASE_URL}/projects/{GITLAB_PROJECT_ID}/merge_requests?source_branch={branch_name}&private_token={GITLAB_API_TOKEN}").json()

			if (len(merge_requests) == 0):
				print("Branch created. Creating merge request...")
				merge_request_creation = requests.post(f"{GITLAB_API_BASE_URL}/projects/{GITLAB_PROJECT_ID}/merge_requests?private_token={GITLAB_API_TOKEN}", 
					data={
						"source_branch":branch_name, 
						"target_branch":"master", 
						"title":f"WIP: Resolve \"{issue_key} - {issue_title}\"",
						"description":f"This merge request was created from this [Jira issue]({issue_link})"
					})
				
				print(merge_request_creation.text)
				print("Merge request created.")

				#Adding to the Jira issue a link to the new merge request
				remote_link_creation = requests.post(f"{JIRA_API_BASE_URL}/issue/{issue_key}/remotelink", headers={"Authorization": f"Basic {ENCODED_JIRA_AUTH}", "Content-Type":"application/json"}, 
					json={
						"object":{
							"url":merge_request_creation.json()["web_url"],
							"title":"Related merge request",
							"icon":{
								"url16x16":"https://about.gitlab.com/images/press/logo/png/gitlab-icon-rgb.png"
							}
						}
					})

		#end of branch creation
		else:
			print(f"Branch {branch_name} already exists.")

		print(' ') #Skip a line

	#end of checking issue in progress
#end of for loop
